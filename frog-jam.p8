pico-8 cartridge // http://www.pico-8.com
version 32
__lua__

--frog jam
--d4rkonion

function print_centered(str, y, col)
	print(str, 64 - (#str * 2), y, col) 
end

--from https://www.lexaloffle.com/bbs/?pid=80095
-- linefill x0 y0 x1 y1 r [col]
-- draw a tick line
function linefill(ax,ay,bx,by,r,c)
	if(c) color(c)
	local dx,dy=bx-ax,by-ay
-- avoid overflow
	-- credits: https://www.lexaloffle.com/bbs/?tid=28999
local d=max(abs(dx),abs(dy))
local n=min(abs(dx),abs(dy))/d
d*=sqrt(n*n+1)
	if(d<0.001) return
	local ca,sa=dx/d,-dy/d
 
	-- polygon points
	local s={
	 {0,-r},{d,-r},{d,r},{0,r}
	}
	local u,v,spans=s[4][1],s[4][2],{}
	local x0,y0=ax+u*ca+v*sa,ay-u*sa+v*ca
	for i=1,4 do
			local u,v=s[i][1],s[i][2]
			local x1,y1=ax+u*ca+v*sa,ay-u*sa+v*ca
			local _x1,_y1=x1,y1
			if(y0>y1) x0,y0,x1,y1=x1,y1,x0,y0
			local dx=(x1-x0)/(y1-y0)
			if(y0<0) x0-=y0*dx y0=-1
			local cy0=y0\1+1
			-- sub-pix shift
			x0+=(cy0-y0)*dx
			for y=y0\1+1,min(y1\1,127) do
					-- open span?
					local span=spans[y]
					if span then
							rectfill(x0,y,span,y)
					else
							spans[y]=x0
					end
					x0+=dx
			end
			x0,y0=_x1,_y1
	end

end


------- Frog Stuff --------
frog={
	currentphase=1,
	waitstep=-1,
	deathstep=0,
	phases={
		{"eye"},
		{"mouth"},
		{"nose"},
		{"eye", "mouth"},
		{"nose", "mouth"},
		{"eye", "nose", "mouth"},
	},
	eyes={
		{
			id=1,
			name="left",
			x=52,
			y=0,
			w=4,
			h=4,
			health=12,
			lastshot=0,
			damaged=false
		},
		{
			id=2,
			name="right",
			x=68,
			y=0,
			w=4,
			h=4,
			health=12,
			lastshot=15,
			damaged=false
		}
	},
	mouth={
		x=56,
		y=14,
		w=10,
		h=4,
		step=0,
		health=40,
		damaged=false
	},
	nose={
		x=54,
		y=7,
		w=5,
		h=1,
		lastshot=0,
		health=40,
		damaged=false
	}
}

function draw_frog()
	palt(0, false)
	palt(15, true)
	map(0)
	
	--left eye
	foreach(frog.eyes, function(eye)
		if eye.health < 1 then
			pal(9, 13)
			pal(3, 1)
		elseif eye.damaged then
			pal(9, 7)
			pal(3, 6)
			eye.damaged=false
		end
		spr(7, eye.x,0)
		pal()
		palt(0, false)
		palt(15, true)
	end)


	--mouth
	if frog.mouth.damaged then
		pal(0, 13)
		frog.mouth.damaged=false
	end
	if frog.mouth.health < 1 then
		spr(24, frog.mouth.x,frog.mouth.y)
		spr(25, 64,14)
	else
		spr(8, frog.mouth.x,frog.mouth.y)
		spr(9, 64,14)
	end



	--open mouth
	local gap=0
	if frog.mouth.step > 0 then
		if frog.mouth.step > 140 then
			gap=1
		elseif frog.mouth.step > 130 then
			gap=2
		elseif frog.mouth.step > 50 then
			gap=3
		else
			gap=flr(frog.mouth.step/20)
		end
		rectfill(frog.mouth.x,18-gap, 71,18+gap, 0)
		circfill(frog.mouth.x,18-gap,1,11)
		circfill(71,18-gap,1,11)
	end
	pal()
  palt(0, false)
	palt(15, true)

	--nose
	if frog.nose.damaged then
		pal(0, 13)
		frog.nose.damaged=false
	end

	if frog.nose.health < 1 then
		spr(11, 60,7)
	else
		spr(10, 60,7)
	end
	pal()
	palt(0, false)
	palt(15, true)

	palt()   
end

function update_frog()
	--do phase attacks
	foreach(frog.phases[frog.currentphase], function(attacktype)
		--eye attacks
		if (attacktype == "eye") then
			foreach(frog.eyes, function(eye)
				if (eye.health < 1) return false
				if (eye.lastshot > 30) then
					make_attack("eye", eye)
					eye.lastshot=0
				else
					eye.lastshot+=1
				end
			end)
		elseif (attacktype == "mouth") then
			if frog.mouth.health < 1 then
				if frog.mouth.step==150 then
					frog.mouth.step=0
				elseif frog.mouth.step>0 then
					frog.mouth.step+=1
				end
			else
				frog.mouth.step+=1
				if (frog.mouth.step == 60) make_attack("mouth") sfx(1)
				if (frog.mouth.step >150) frog.mouth.step=0
			end
		elseif (attacktype == "nose") then
			if (frog.nose.health < 1) return false
			if (frog.nose.lastshot > 100) then
				sfx(2)
				for i=1, 20 do
					make_attack("nose")
				end
				frog.nose.lastshot=0
			else
				frog.nose.lastshot+=1
			end
		end
	end)
end

function kill_frog()
	if (frog.deathstep < 99) then
		make_particle(rnd(40)+40,rnd(24),0,0, 2, 3,{flr(rnd(2))+6,},rnd(7) +3)
		if(frog.deathstep % 12 == 0) sfx(4)
	elseif (frog.deathstep == 100) then
		make_particle(rnd(40)+40,rnd(24),0,0, 2, 50	,{7},200)
		sfx(5)
		local splodes={{x=20, y=20}, {x=20, y=100}, {x=100, y=20}, {x=100, y=100}, {x=64, y=64}}
		foreach(splodes, function(splode)
			for i=1, 20 do
				make_particle(splode.x,splode.y,rnd(4)-2,rnd(4)-2,2,100,{3, 11, 9, 11}, 1)
			end
		end)		
	end
end

attacks={}
function make_attack(_type, _sourceref)
	local _x
	local _y
	local _dx
	local _dy
	local _w
	local _h
	local _angle
	local _hurtxoff=0
	local _hurtyoff=0

	if (_type=="eye") then
		_x = frog.eyes[_sourceref.id].x
		_y = 6
		angle=atan2(player.y - _y, player.x -_x)
		_dx=sin(angle)*2
  	_dy=cos(angle)*2
		_w=3
		_h=4
	elseif (_type=="mouth") then
		_x = 64
		_y = 16
		angle=atan2(player.y - _y+1, player.x -_x+4)
		_dx=sin(angle)*2
  	_dy=cos(angle)*2
		_w=3
		_h=3
		_hurtxoff=2
		_hurtyoff=1		
	elseif (_type=="nose") then
		_x = 64
		_y = 10
		angle=flr(rnd(40)-20) /100
		_dx=sin(angle)*(flr(rnd(20)) +4 ) /10
  	_dy=cos(angle)*(flr(rnd(20)) +4 ) /10
		_w=3
		_h=1
		_hurtxoff=2
		_hurtyoff=1
	end

	local attack = {
		x = _x,
		y = _y,
		dx=_dx,
		dy=_dy,
		w=_w,
		h=_h,
		type=_type,
		hurtxoff=_hurtxoff,
		hurtyoff=_hurtyoff
	}
	add(attacks, attack)
end

function update_attack(_atk)

	--move attack
	if (_atk.type=="eye") then
		_atk.x+=_atk.dx*0.6
		_atk.y+=_atk.dy*0.6
	elseif (_atk.type=="mouth") then
		if frog.mouth.step < 90 then
			_atk.x+=_atk.dx*3
			_atk.y+=_atk.dy*3
		else
			_atk.x-=_atk.dx*3
			_atk.y-=_atk.dy*3
		end
	elseif (_atk.type=="nose") then
		_atk.x+=_atk.dx*0.6
		_atk.y+=_atk.dy*0.6
	end

	--hurt player
	local dx = _atk.x- player.x
	local dy = _atk.y- player.y
	if (abs(dx) < _atk.w+_atk.hurtxoff) then
		if (abs(dy) < _atk.h+_atk.hurtyoff ) then
			player.health-=1
			if (player.health > 0) then
				for i=1, 20 do
					make_particle(player.x,player.y,rnd(2)-1,rnd(2)-1,2,10,{13, 5, 6, 14}, 1)
				end
			end
			del(attacks, _atk)
		end
	end
	if (_atk.y > 128 and _atk.type == "eye") del(attacks, _atk)
	if (_atk.type == "mouth" and frog.mouth.step == 119) del(attacks, _atk)
end

function draw_attack(_atk)
	palt(0, false)
	palt(15, true)
	if (_atk.type=="eye") then
		spr(23, _atk.x,_atk.y)
	elseif (_atk.type=="mouth") then
		-- tongue
		linefill(63,17, _atk.x,_atk.y,2,0)
		linefill(63,17, _atk.x,_atk.y,1,8)
		circfill(_atk.x,_atk.y, 4,0)
		circfill(_atk.x,_atk.y, 3,8)
	elseif (_atk.type=="nose") then
		spr(26, _atk.x, _atk.y)
	end

	
	palt()
end
------- Player Stuff -------
player={
	x=64,
	y=64,
	w=8,
	h=8,
	frame=0,
	step=0,
	lastshot=-1,
	health=3
}

function update_player()
	if (player.health <= 0) return false
	if (btn(⬅️)) player.x-=1
	if (btn(➡️)) player.x+=1
	if (btn(⬇️)) player.y+=1
	if (btn(⬆️)) player.y-=1

	-- advance animation according to speed
	player.step+=1
	if(player.step%1==0) player.frame+=1
	if(player.frame>2) player.frame=0

	if btn(🅾️) and player.lastshot == -1 then
		make_shot(player.x, player.y)
		sfx(0)
		player.lastshot=0
	end

	if player.lastshot > 3 then
		player.lastshot=-1
	elseif player.lastshot != -1 then
		player.lastshot+=1
	end

end

function draw_player()
	if (player.health <= 0) return false
	palt(0, false)
	palt(15, true)
	spr(player.frame+64, player.x, player.y)
	palt()
end

function kill_player()
	for i=1, 20 do
		make_particle(player.x+8,player.y+8,rnd(4)-2,rnd(4)-2,2,30,{13, 5, 6, 14}, 1)
	end
	sfx(6)
	player.x=64
	player.y=200
	player.health=-1
	
end



shots={}
function make_shot(_x, _y)
	local shot = {
		x = _x,
		y = _y,
		w=1,
		h=1,
	}
	add(shots, shot)
end

function draw_shot(shot)
	palt(0, false)
	palt(15, true)
	spr(67, shot.x, shot.y)
	palt()
end

function update_shot(shot)
	shot.y-=3
	local checkeye=false
	local checknose=false
	local checkmouth=false

	foreach(frog.phases[frog.currentphase], function(bodypart)
		if (bodypart=="eye") checkeye=true
		if (bodypart=="nose") checknose=true
		if (bodypart=="mouth") checkmouth=true
	end)

	--handle hitting the frog's eyes
	if checkeye then
		foreach(frog.eyes, function(eye)
			local dx = shot.x - eye.x
			local dy = shot.y - eye.y
			if (abs(dx) < shot.w+eye.w) then
				if (abs(dy) < shot.h+eye.h) then
					eye.health-=1
					eye.damaged=true
					del(shots, shot)
				end
			end
		end)
	end

	
	--handle hitting the frog's mouth
	if checkmouth and frog.mouth.health > 0 then
		local dx = shot.x - frog.mouth.x -6
			local dy = shot.y - frog.mouth.y
			if (abs(dx) < shot.w+frog.mouth.w) then
				if (abs(dy) < shot.h+frog.mouth.h) then
					frog.mouth.health-=1
					frog.mouth.damaged=true
					del(shots, shot)
				end
			end

		if (shot.y < -10) del(shots, shot)
	end

	--handle hitting the frog's mouth
	if checknose and frog.nose.health > 0 then
		local dx = shot.x - frog.nose.x -6
			local dy = shot.y - frog.nose.y
			if (abs(dx) < shot.w+frog.nose.w) then
				if (abs(dy) < shot.h+frog.nose.h) then
					frog.nose.health-=1
					frog.nose.damaged=true
					del(shots, shot)
				end
			end

		if (shot.y < -10) del(shots, shot)
	end
end


------- Particle Stuff -------
particles={}
function make_particle(_x,_y,_dx,_dy,_type,_maxage,_colarr,_s)
	local _p = {
		x=_x,
		y=_y,
		dx=_dx,
		dy=_dy,
		type=_type,
		maxage=_maxage,
		age=0,
		col=_colarr[1],
		colarr=_colarr,
		s=_s,
		os=_s
	}
	add(particles, _p)
end

function draw_particle(_p)
	circfill(_p.x,_p.y, _p.s, _p.colarr[ ceil(rnd(#_p.colarr)) ])	
end

function update_particle(_p)
	if _p.age >= _p.maxage then
		-- remove old particles
		 del(particles, _p)
	else
		--move the particles
		_p.x = _p.x + _p.dx
		_p.y = _p.y + _p.dy
		_p.age+=1
	end
end

------- Game Stuff --------
game={
	state="menu"
}
function restart_game()
	frog={
		currentphase=1,
		waitstep=-1,
		deathstep=0,
		phases={
			{"eye"},
			{"mouth"},
			{"nose"},
			{"eye", "mouth"},
			{"nose", "mouth"},
			{"eye", "nose", "mouth"},
		},
		eyes={
			{
				id=1,
				name="left",
				x=52,
				y=0,
				w=4,
				h=4,
				health=20,
				lastshot=0,
				damaged=false
			},
			{
				id=2,
				name="right",
				x=68,
				y=0,
				w=4,
				h=4,
				health=20,
				lastshot=15,
				damaged=false
			}
		},
		mouth={
			x=56,
			y=14,
			w=10,
			h=4,
			step=0,
			health=40,
			damaged=false
		},
		nose={
			x=54,
			y=7,
			w=5,
			h=1,
			lastshot=0,
			health=40,
			damaged=false
		}
	}
	player={
		x=64,
		y=64,
		w=8,
		h=8,
		frame=0,
		step=0,
		lastshot=-1,
		health=3
	}
	shots={}
	attacks={}
	game.state="fight"

end

------- PICO-8 Stuff -------
function _init()
	
end


function _draw()
	
	cls(12)
	
	if (game.state=="menu") then
		palt(0, false)
		palt(15, true)
		local t1 = t()*20
		local x = 1 + sin(t1/90)*4
		local y = 1 + sin(t1/50)*2
		rectfill(51+x,22+y, 60+x,23+y, 1)
		rectfill(65+x,22+y, 74+x,23+y, 1)
		
		spr(7, 52+x,15+y)
		spr(7, 66+x,15+y)
		palt()
		rectfill(20,45, 108,100, 1)
		print_centered("froggy hell", 50, 3)
		line(35,57, 93,57, 3)
		
		print("press ❎ and 🅾️", 35,62, 3)
		print_centered("to start", 69, 3)

		print_centered("mini jam: frogs", 90, 3)
	elseif (game.state=="fight") then
		draw_frog()
		draw_player()
		foreach(shots, draw_shot)
		foreach(attacks, draw_attack)

		--draw health remaining
		for i=0, player.health-1 do
			palt(0, false)
			palt(15, true)
			spr(80, 2+i*9,118)
		end
		foreach(particles, draw_particle)
		if (player.health <=0 ) then
			print_centered("hard luck!", 40, 7)
			print("press ❎ and 🅾️", 35,62, 7)
			print_centered("to restart", 69, 7)
		end
	elseif (game.state=="credits") then
		draw_player()
		print_centered("thanks for playing!", 40, 7)
		print_centered("made in 17 hours for", 50, 7)
		print_centered("mini jam: frogs", 60, 7)
		print_centered("@d4rk_onion", 90, 7)
	end
	
	


end

function _update()
	

	if game.state=="menu" then
		if btn(🅾️) and btn(❎) then
			game.state="fight"
			music(1)
		end
	elseif (game.state=="fight") then

		update_player()
		update_frog()
		foreach(attacks, update_attack)
		foreach(shots, update_shot)
		foreach(particles, update_particle)

		if player.health == 0 then
			kill_player()
			music(-1)
		elseif player.health <= -1 and btn(🅾️) and btn(❎) then
			restart_game()
			music(1)
		end

		if frog.currentphase==1 and frog.eyes[1].health < 1 and frog.eyes[2].health < 1 then
			frog.currentphase+=0.5
		elseif frog.currentphase==2 and frog.mouth.health < 1 then
			frog.currentphase+=0.5
		elseif frog.currentphase==3 and frog.nose.health < 1 then
			frog.currentphase+=0.5
		elseif frog.currentphase==4 and frog.mouth.health < 1 and frog.eyes[1].health < 1 and frog.eyes[2].health < 1 then
			frog.currentphase+=0.5
		elseif frog.currentphase==5 and frog.mouth.health < 1 and frog.nose.health < 1 then
			frog.currentphase+=0.5
		elseif frog.currentphase==6 and frog.nose.health < 1 and frog.mouth.health < 1 and frog.eyes[1].health < 1 and frog.eyes[2].health < 1 then
			if frog.deathstep < 149 then
				kill_frog()
				frog.deathstep+=1
			else 
				game.state="credits"
				music(0)
			end
		end
		
		if frog.waitstep == 30 then
			frog.currentphase+=0.5
			frog.waitstep=0
			frog.eyes[1].health=20
			frog.eyes[2].health=20
			frog.mouth.health=40
			frog.mouth.step=0
			frog.nose.health=40
			foreach(attacks, function(atk) if (atk.type=="mouth") then del(attacks, atk)end end)
		elseif frog.currentphase % 1 == 0.5 then
			frog.waitstep+=1
		end
	elseif (game.state=="credits") then
		update_player()
	end

	
	
end

__gfx__
00000000cccccccc0bbbbbbbbbbbbbbbbbbbbbbb33333330ccccccccffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000
00000000ccccccc0bbbbbbbbbbbbbbbbbbbbbbbbb33333330cccccccffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000
00000000cccccc0bbbbbbbbbbbbbbbbbbbbbbbbbbbb3333330ccccccffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000
00000000cccccc0bbbbbbbbbbbb0000000000bbbbbb3333330ccccccff0000ff00ffffffffffff00f0ffff0ff011110f00000000000000000000000000000000
00000000cccccc0bbbbbbbbbbbbbbbbbbbbbbbbbbbb3333330ccccccf033330ff00000000000000ff0ffff0ff011110f00000000000000000000000000000000
00000000cccccc0bbbbbbbbbbbbbbbbbbbbbbbbbbbbb333330cccccc03999930ffffffffffffffff0ffffff001111dd000000000000000000000000000000000
00000000ccccc00bbbbbbbbbbbbbbbbbbbbbbbbbbbbb333330cccccc09000090fffffffffffffffff00ff00ff001d00f00000000000000000000000000000000
00000000cccc00bbbbbbbbbbbbbbbbbbbbbbbbbbbbbb3333330ccccc09999990ffffffffffffffffff0ff0ffff01d0ff00000000000000000000000000000000
ccccccccccc030bbbbb00000bb000000000000bb000003333300ccccff0000fffffffffffffffffff88fffff0000000000000000000000000000000000000000
cccccccccc0330bbbb03333bbbbbbbbbbbbbbbbbbbbbb03333030cccf099990fffffffffffffffff8228ffff0000000000000000000000000000000000000000
ccccccccc03330bbb033bbbbbbbbbbbbbbbbbbbbbbbbbb03330330cc09999990ff000000000000ff8228ffff0000000000000000000000000000000000000000
ccccccccc03330bbb033bbbbbbbbbbbbbbbbbbbbbbbbbb033303330c0900009000ffffffffd11100f88fffff0000000000000000000000000000000000000000
cccccccc03330bbbb03bbbbbbbbbbbbbbbbbbbbbbbbbbb033330330c099009900fffffffffd11110ffffffff0000000000000000000000000000000000000000
cccccccc03330bbbb03bbbbbbbbbbbbbbbbbbbbbbbbbbb033330333009999990fffffffffffd111dffffffff0000000000000000000000000000000000000000
cccccccc03330bbbb03bbbbbbbbbbbbbbbbbbbbbbbbbbb0333303330f099990fffffffffffffdddfffffffff0000000000000000000000000000000000000000
cccccccc03330bbbb0bbbbbbbbbbbbbbbbbbbbbbbbbbbb0b33303330ff0000ffffffffffffffffffffffffff0000000000000000000000000000000000000000
0000000003330bbbbb0bbbbbbbbbbbbbbbbbbbbbbbbbb0bbb3303330000000000000000000000000000000000000000000000000000000000000000000000000
00000000033330bbbbb0bbbbbbbbbbbbbbbbbbbbbbbb0bbbb3033330000000000000000000000000000000000000000000000000000000000000000000000000
00000000033330bbbbbb0bbbbbbbbbbbbbbbbbbbbbb0bbbbb3033330000000000000000000000000000000000000000000000000000000000000000000000000
00000000c033330bbbbbb00bbbbbbbbbbbbbbbbbb003333bb033330c000000000000000000000000000000000000000000000000000000000000000000000000
00000000c033330bbbb0033000bbbbbbbbbbbb00033003333033330c000000000000000000000000000000000000000000000000000000000000000000000000
00000000cc03330bbbbbb0333300000000000033330bbbbb303330cc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccc0330bbbbbbb00333333333333333300bbbbbb30330ccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000cccc030bbbbbbb330333333333333330bbbbbbbb3030cccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccc030bbbbbbbb303333333333330b333bbbb3030ccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000cccccc030bbbbb00b3033333333330bb0033bb3030cccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccccc00000bbb000333333333333000bbb00000ccccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccccccc003000333333333333333333000300ccccccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccccccccc00333333333333333333333300ccccccccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000ccccccccccccc0000033333333333300000ccccccccccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000cccccccccccccccccc000000000000cccccccccccccccccc000000000000000000000000000000000000000000000000000000000000000000000000
00000000cccccccccccccccccccccccccccccccccccccccccccccccc000000000000000000000000000000000000000000000000000000000000000000000000
ffe00effffe00effdfe00efdffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
ff0000ffff0000ff6d0000d6ffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
fff00fffdff00ffd66f00f66fff0ffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
ff6666ff6d6666d6f666666fff0e0fff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
f66ee66ff66ee66ffffeeffffff0ffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
66000066ff0000ffff0000ffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6d0000d6ff0000ffff0000ffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
dff00ffdfff00ffffff00fffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
ffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
f00ff00f000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08800880000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
08888880000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
f088880f000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
ff0880ff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
fff00fff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__gff__
0011000505000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
0000000000010203040506000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000111213141516000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000212223242526000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000313233343536000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
000100000000000000000000155001550015500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000200002125021250202501f2501e2501d2501c2501a25018250172501625015250142501425014250132501325013250122501225011250112501025010250102500f2500e2500e2500d2500c2500a25009250
00020000207532375326753287532b7532e75330753337533575336753377533875338753377533675334753347533375331753307532e7532d7532c7532b753297532875326753257532375322753207531e753
180100001b0501b0501b0501b0501b0501c0502005020050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
01020000306203162031620316203262033620336203262031620306202f6202d6202a6202962026620226201f6201c62017620136200f6200b6200a620076200562003620026200262000600006000060000600
00070000306203162031620316203262033620336203262031620306202f6202d6202a6202962026620226201f6201c62017620136200f6200b6200a620076200562003620026200262001610016100061000610
010400001c0521c0521c0521c0521c0521c05218052180521805218052180521805218052180521a0521a0521a0521a0521a0521a052170521705217052170521705217052170521705218052180521805218052
0110000018150000001815009100211501d150000001a150181500000018150181501815016100181521915218151181511c100221501c1000110025150000000000027150000000000000000000000000000000
011000000c155000000c1550c155131550c155000050000513155131550c15500000000000c1550c1550d1550c155000000000000000000000d10000000000000f10000000000000000000000000000000000000
01100000187530000018753187531865518753000001875318753000001875318753186551865300000000000f000276000f000276500f00027300276500f0000f000276500f0000f0000f0000f0000f0040f000
010f0000180530000000000000001805300000000003f603180530000000000000001805300000000003f603180530000000000000001805300000000003f603180530000000000000001805300000000003f603
010f00001f055260551f055220551f055260551f055220551f055260551f055220551f055260551f055220551b0551b05521055220551b0551b05521055220551d0551d05521055220551e0551e0552205521055
010f000013055000001305513055130551a0001305513055130551a0001305513055130551300013055130550f0550f0000f0550f0550f0550f0000f0550f0551105511000110551105511055120001105511055
010f0000180530000019600196002465300000000003f603180530000000000000002465300000000003f603180530000000000000002465300000000003f603180530000000000000002465300000000003f603
010f00001f055260551f055220551f055260551f055220551f055260551f055220551f055260551f055220551b0551b05521055220551b0551b0552105522055160511705118051190511a0511c0511d0511e051
010f00001f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0521f0522205122052220522205222052220522105121052210522105221052210521f0511f0521f0521f052
010f00001b0511b0521b0521b0521b0521b0521b0521b0521b0521b0521b0521b0521b0521b0521b0521b0522205122052220522205222052220522105121052210522105221052210521e0511e0521e0521e052
010f000013055000001305513055130551a0001305513055130551a00013055130551305513000130551305511055000001105511055110551a000110551105511055000001105511055110551a0001105511055
010f00000f055000000f0550f0550f0551a0000f0550f0550f0551a0000f0550f0550f055130000f0550f05511055000001105511055110551a000110551105512055000001205512055120551a0001205512055
010f0000260512605226052260522605226052260522605226052260522605226052260522605226052260522a0512a0522a0522a0522a0522a0522a0522a0522a0522a0522a0522a0522a0522a0522a0522a052
490f00002b0522b0522b0522b0522b0422b0422b0422b0422b0322b0322b0322b0322b0222b0222b0222b0222b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b0122b012
__music__
04 07080944
00 41420c4a
01 41420c0a
00 810b0c0a
00 410b0c0a
00 410b0c0d
00 410e0c0d
00 410f110d
00 4110120d
00 410f110d
00 4113120d
02 41140c0a

